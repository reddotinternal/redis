package com.db.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalRedisApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinalRedisApiApplication.class, args);
	}

}
